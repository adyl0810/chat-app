USE `chat`;

CREATE TABLE `chat_messages`
(
    `id`       INT AUTO_INCREMENT NOT NULL,
    `chat_id`  INT                NOT NULL,
    `user_id`  INT                NOT NULL,
    `text`     VARCHAR(256)       NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_chat_messages__chat`
        FOREIGN KEY (`chat_id`)
            REFERENCES `chats` (`id`),
    CONSTRAINT `fk_chat_messages__user`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
);