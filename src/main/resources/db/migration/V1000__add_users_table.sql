USE `chat`;

CREATE TABLE `users`
(
    `id`       INT AUTO_INCREMENT NOT NULL,
    `email`    VARCHAR(128)       NOT NULL,
    `password` VARCHAR(128)       NOT NULL,
    `username` VARCHAR(128)       NOT NULL DEFAULT 'No Name',
    `active`   BOOLEAN            NOT NULL DEFAULT TRUE,
    `role`     VARCHAR(24)        NOT NULL DEFAULT 'USER',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `email_unique` (`email` ASC)
);