USE `chat`;

CREATE TABLE `chat_users`
(
    `id`       INT AUTO_INCREMENT NOT NULL,
    `chat_id`  INT                NOT NULL,
    `user_id`  INT                NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_chat_users__chat`
        FOREIGN KEY (`chat_id`)
            REFERENCES `chats` (`id`),
    CONSTRAINT `fk_chat_users__user`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
);