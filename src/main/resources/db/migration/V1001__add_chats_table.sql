USE `chat`;

CREATE TABLE `chats`
(
    `id`       INT AUTO_INCREMENT NOT NULL,
    `name`     VARCHAR(128)       NOT NULL,
    PRIMARY KEY (`id`)
);