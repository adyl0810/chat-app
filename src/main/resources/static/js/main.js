$(async () => {

    $(function () {
        const token = $("meta[name='_csrf']").attr("content");
        const header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function(e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });
    });

    const container = $('#container')
    const chatsContainer = $('#chats-container')

    const buildAllChatsHtml = (arr) => {
        chatsContainer.empty()
        for (let i = 0; i < arr.length; i++) {
            const {id, name} = arr[i]
            const chat = `
            <div class="chat-container">
                <h5>Chat Id: ${id}, Chat name: ${name}</h5>
                <form class="open-chat-form">
                    <button>Open Chat</button>
                </form>
                <div class="messages-container">
                
                </div>
                <form class="send-message-form" hidden>
                    <textarea name="messageInput" class="message-input" cols="30" rows="10" placeholder="Type your message here"></textarea>
                    <button>Send Message</button>
                </form>
            </div>
        `
            chatsContainer.append(chat)
            $($('.open-chat-form')[i]).on('submit', async (e) => {
                e.preventDefault()
                $($('.send-message-form')[i]).removeAttr('hidden')
                $($('.messages-container')[i]).empty()
                await $.ajax({
                    type : 'GET',
                    url : `http://localhost:8080/api/chats/messages/${id}`,
                }).then((data) => {
                    let oldData = data
                    for (let j = 0; j < data.length; j++) {
                        const {id, chat, user, text} = data[j]
                        const message = `
                            <div class="message-container">
                                <p><b>${user.username}</b>: ${text}</p>
                            </div>
                        `
                        $($('.messages-container')[i]).append(message)

                    }
                    setInterval(function () {
                        $.ajax({
                            type : 'GET',
                            url : `http://localhost:8080/api/chats/messages/${id}`,
                        }).then(data => {
                            if (data !== oldData) {
                                oldData = data
                                $($('.messages-container')[i]).empty()
                                for (let j = 0; j < data.length; j++) {
                                    const {id, chat, user, text} = data[j]
                                    const message = `
                            <div class="message-container">
                                <p><b>${user.username}</b>: ${text}</p>
                            </div>
                        `
                                    $($('.messages-container')[i]).append(message)

                                }
                            }
                        })
                    }, 1000)
                })
            })
            $($('.send-message-form')[i]).on('submit', async (e) => {
                e.preventDefault()

                await $.ajax({
                    type : 'POST',
                    url : `http://localhost:8080/api/chats/messages/`,
                    data : {
                        chatId : i+1,
                        userId : $('#user-id').val(),
                        text : $($('.message-input')[i]).val()
                    }
                })
                $($('.message-input')[i]).val('')
                $($('.messages-container')[i]).empty()
                await $.ajax({
                    type : 'GET',
                    url : `http://localhost:8080/api/chats/messages/${id}`,
                }).then((data) => {
                    for (let j = 0; j < data.length; j++) {
                        const {id, chat, user, text} = data[j]
                        const message = `
                            <div class="message-container">
                                <p><b>${user.username}</b>: ${text}</p>
                            </div>
                        `
                        $($('.messages-container')[i]).append(message)
                    }
                })
            })
        }
    }

    const getAllChats = async () => {
        await $.ajax({
            type : 'GET',
            url : 'http://localhost:8080/api/chats'
        }).then((data) => {
            buildAllChatsHtml(data)
        })
    }


    await getAllChats()

    $('#createChatForm').on('submit', async (e) => {
        e.preventDefault()
        await $.ajax({
            type : 'POST',
            url : 'http://localhost:8080/api/chats',
            data : {
                name : $('#createChatInput').val()
            }
        })
        await getAllChats()
    })
})

