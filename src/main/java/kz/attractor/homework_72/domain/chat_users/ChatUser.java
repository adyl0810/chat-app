package kz.attractor.homework_72.domain.chat_users;

import kz.attractor.homework_72.domain.chat.Chat;
import kz.attractor.homework_72.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Table(name = "chat_users")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class ChatUser {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Chat chat;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public static ChatUser from(ChatUserDTO chatUsers) {
        return builder()
                .id(chatUsers.getId())
                .chat(Chat.from(chatUsers.getChat()))
                .user(User.from(chatUsers.getUser()))
                .build();

    }
}
