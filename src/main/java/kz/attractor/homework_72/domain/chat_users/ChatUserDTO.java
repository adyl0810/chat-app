package kz.attractor.homework_72.domain.chat_users;

import kz.attractor.homework_72.domain.chat.ChatDTO;
import kz.attractor.homework_72.domain.user.UserDTO;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class ChatUserDTO {
    @NotNull
    private Integer id;

    @NotNull
    private ChatDTO chat;

    @NotNull
    private UserDTO user;

    public static ChatUserDTO from(ChatUser chatUser) {
        return builder()
                .id(chatUser.getId())
                .chat(ChatDTO.from(chatUser.getChat()))
                .user(UserDTO.from(chatUser.getUser()))
                .build();

    }
}
