package kz.attractor.homework_72.domain.chat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ChatService {
    private final ChatRepository chatRepository;

    public List<ChatDTO> getChats() {
        List<Chat> chats = chatRepository.findAll();
        return fromListToDtoList(chats);
    }

    public ChatDTO getChat(Integer id) {
        Chat chat = chatRepository.findById(id).orElseThrow();
        return ChatDTO.from(chat);
    }

    public void addChat(String name) {
        ChatDTO chat = new ChatDTO(name);
        chatRepository.save(Chat.from(chat));
    }

    public List<ChatDTO> fromListToDtoList(List<Chat> chats) {
        List<ChatDTO> chatDTOs = new ArrayList<>();
        for (Chat chat : chats) {
            chatDTOs.add(ChatDTO.from(chat));
        }
        return chatDTOs;
    }
}
