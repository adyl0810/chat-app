package kz.attractor.homework_72.domain.chat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chats")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ChatRestController {

    private final ChatService chatService;

    @GetMapping
    public List<ChatDTO> getChats() {
        return chatService.getChats();
    }

    @PostMapping
    public void addChat(@RequestParam String name) {
        chatService.addChat(name);
    }
}
