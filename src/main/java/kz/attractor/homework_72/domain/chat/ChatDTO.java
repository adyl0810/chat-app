package kz.attractor.homework_72.domain.chat;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class ChatDTO {

    @NotNull
    private int id;

    @NotBlank
    @Size(min = 2, max = 128)
    private String name;

    public ChatDTO(String name) {
        this.name = name;
    }

    public static ChatDTO from(Chat chat) {
        return builder()
                .id(chat.getId())
                .name(chat.getName())
                .build();
    }
}
