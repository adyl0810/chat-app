package kz.attractor.homework_72.domain.chat;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Table(name = "chats")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Chat {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 2, max = 128)
    @Column(length = 128)
    private String name;

    public static Chat from(ChatDTO chat) {
        return builder()
                .id(chat.getId())
                .name(chat.getName())
                .build();
    }


}


