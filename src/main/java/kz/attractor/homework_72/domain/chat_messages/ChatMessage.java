package kz.attractor.homework_72.domain.chat_messages;

import kz.attractor.homework_72.domain.chat.Chat;
import kz.attractor.homework_72.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Data
@Table(name = "chat_messages")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class ChatMessage {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Chat chat;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @NotNull
    @Max(256)
    @Column(length = 256)
    private String text;

    public static ChatMessage from(ChatMessageDTO chatMessage) {
        return builder()
                .id(chatMessage.getId())
                .chat(Chat.from(chatMessage.getChat()))
                .user(User.from(chatMessage.getUser()))
                .text(chatMessage.getText())
                .build();
    }
}
