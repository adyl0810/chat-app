package kz.attractor.homework_72.domain.chat_messages;

import kz.attractor.homework_72.domain.chat.ChatDTO;
import kz.attractor.homework_72.domain.chat.ChatService;
import kz.attractor.homework_72.domain.user.UserDTO;
import kz.attractor.homework_72.domain.user.UserService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ChatMessageService {

    private final ChatMessageRepository chatMessageRepository;
    private final ChatService chatService;
    private final UserService userService;

    public List<ChatMessageDTO> getChatMessages() {
        List<ChatMessage> messages = chatMessageRepository.findAll();
        return fromListToDtoList(messages);
    }

    public void addMessageToChat(Integer chatId, Integer userId, String text) {
        ChatDTO chat = chatService.getChat(chatId);
        UserDTO user = userService.getUser(userId);
        ChatMessageDTO message = new ChatMessageDTO(chat, user, text);
        chatMessageRepository.save(ChatMessage.from(message));
    }

    public List<ChatMessageDTO> getMessagesByChatId(Integer id) {
        List<ChatMessage> messages = chatMessageRepository.findAllByChatId(id);
        return fromListToDtoList(messages);
    }

    public List<ChatMessageDTO> fromListToDtoList(List<ChatMessage> messages) {
        List<ChatMessageDTO> messageDTOs = new ArrayList<>();
        for (ChatMessage message : messages) {
            messageDTOs.add(ChatMessageDTO.from(message));
        }
        return messageDTOs;
    }
}
