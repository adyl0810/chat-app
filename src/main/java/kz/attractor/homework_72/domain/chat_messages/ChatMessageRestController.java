package kz.attractor.homework_72.domain.chat_messages;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chats/messages")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ChatMessageRestController {

    private final ChatMessageService chatMessageService;

    @GetMapping("/{id}")
    public List<ChatMessageDTO> getMessagesFromChat(@PathVariable Integer id) {
        return chatMessageService.getMessagesByChatId(id);
    }

    @PostMapping
    public void addMessageToChat(@RequestParam Integer chatId,
                                 @RequestParam Integer userId,
                                 @RequestParam String text) {
        chatMessageService.addMessageToChat(chatId, userId, text);
    }
}
