package kz.attractor.homework_72.domain.chat_messages;

import kz.attractor.homework_72.domain.chat.ChatDTO;
import kz.attractor.homework_72.domain.user.UserDTO;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class ChatMessageDTO {

    @NotNull
    private Integer id;

    @NotNull
    private ChatDTO chat;

    @NotNull
    private UserDTO user;

    @NotNull
    private String text;

    public ChatMessageDTO(ChatDTO chat, UserDTO user, String text) {
        this.chat = chat;
        this.user = user;
        this.text = text;
    }

    public static ChatMessageDTO from(ChatMessage chatMessage) {
        return builder()
                .id(chatMessage.getId())
                .chat(ChatDTO.from(chatMessage.getChat()))
                .user(UserDTO.from(chatMessage.getUser()))
                .text(chatMessage.getText())
                .build();
    }
}
