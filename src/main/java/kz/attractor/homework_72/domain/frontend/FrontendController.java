package kz.attractor.homework_72.domain.frontend;

import kz.attractor.homework_72.domain.user.UserDTO;
import kz.attractor.homework_72.domain.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontendController {

    private final UserService userService;

    @GetMapping
    public String index(Principal principal,
                        Model model) {
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        return "index";
    }
}
